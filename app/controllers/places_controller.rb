class PlacesController < ApplicationController

  def index
    @places = Place.paginate(page: params[:page])
  end

  def new
    @place = Place.new
    @placeholder = 'The right JSON format to add a place is: { "name" : "< name of the place >", "description" : "< the description of the place >", "facebook" : "< the link to facebook > ", "instagram" : "< the link to instagram >" }, so now you can paste your json text here...'
  end

  def create
    begin
      json = params[:place][:json]
      hash_place = JSON.parse(json)
      @place = Place.new(name: hash_place['name'],
                    description: hash_place['description'],
                    facebook: hash_place['facebook'],
                    instagram: hash_place['instagram'],
                    json: json)
      @place.save
      flash[:success] = "Place added successfully!"
      redirect_to place_path(@place)
    rescue
      flash[:danger] = "The JSON text provided was in a wrong format. \n The right format is."
      redirect_to addplace_path
    end
  end

  def show
    @place = Place.find(params[:id])
  end

  def edit
  end

  def destroy
    place = Place.find(params[:id])
    place.destroy
    flash[:success] = "Place '#{place.name}' deleted"
    redirect_to places_path
  end
end

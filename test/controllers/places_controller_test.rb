require 'test_helper'

class PlacesControllerTest < ActionDispatch::IntegrationTest
  test "should get places" do
    get places_url
    assert_response :success
  end

  test "should get addplace" do
    get addplace_url
    assert_response :success
  end

end
